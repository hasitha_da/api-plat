<?php

namespace SimitiveApiPlatformBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SimitiveApiPlatformBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
