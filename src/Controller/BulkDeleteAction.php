<?php

namespace SimitiveApiPlatformBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use SimitiveBase\Model\AbstractEntity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BulkDeleteAction extends AbstractBulkOperationAction
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent(), true);
        $resourceClass = $request->attributes->get('_api_resource_class');
        $filters = $content['filters'] ?? null;

        return $this->process($resourceClass, $filters);
    }

    protected function process(string $resourceClass, ?array $filters): JsonResponse
    {
        $this->repository = $this->getRepository($this->entityManager, $resourceClass);

        try {
            $entityIds = $this->getEntityIds($resourceClass, $filters);
        } catch (\Exception $e) {
            $this->logger->error($e);

            return new JsonResponse('Error fetching entities for bulk operation.', 500);
        }

        if (empty($entityIds)) {
            throw new BadRequestHttpException('No entities found to bulk delete.');
        }

        $this->entityManager->getConnection()->beginTransaction();

        try {
            $updateCount = $this->applyBulkDelete($entityIds);

            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->entityManager->getConnection()->rollBack();

            return new JsonResponse('Error bulk deleting entities.', 500);
        }

        return new JsonResponse(
            [
                'count' => $updateCount,
                'ids'   => $entityIds,
            ]
        );
    }

    protected function applyBulkDelete(array $entityIds): int
    {
        $count = 0;

        foreach ($entityIds as $entityId) {
            /** @var AbstractEntity $entity */
            $entity = $this->repository->find($entityId);
            $this->entityManager->remove($entity);
            $count++;
        }

        return $count;
    }
}
