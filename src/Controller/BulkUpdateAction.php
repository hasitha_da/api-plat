<?php

namespace SimitiveApiPlatformBundle\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use SimitiveBase\Helper\FinancialValueConverter;
use SimitiveBase\Model\AbstractEntity;
use SimitiveBase\Model\FinancialValueHolderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExtraAttributesException;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class BulkUpdateAction extends AbstractBulkOperationAction
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        protected LoggerInterface $logger
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent(), true);
        $resourceClass = $request->attributes->get('_api_resource_class');
        $inputData = $content['fields'] ?? null;
        $filters = $content['filters'] ?? null;

        return $this->process($resourceClass, $inputData, $filters);
    }

    protected function process(string $resourceClass, ?array $inputData, ?array $filters): JsonResponse
    {
        if ($inputData === null) {
            throw new BadRequestHttpException(
                "The 'fields' parameter is required."
            );
        }

        $this->repository = $this->getRepository($this->entityManager, $resourceClass);

        try {
            $this->validateInput($inputData, $resourceClass);
        } catch (ExtraAttributesException | BadRequestHttpException $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->logger->error($e);

            return new JsonResponse('Error validating input for bulk operation.', 500);
        }

        try {
            $entityIds = $this->getEntityIds($resourceClass, $filters);
        } catch (\Exception $e) {
            $this->logger->error($e);

            return new JsonResponse('Error fetching entities for bulk operation.', 500);
        }

        if (empty($entityIds)) {
            throw new BadRequestHttpException('No entities found to bulk update.');
        }

        $this->entityManager->getConnection()->beginTransaction();

        try {
            $updateCount = $this->applyBulkUpdate($entityIds, $inputData);

            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->entityManager->getConnection()->rollBack();

            return new JsonResponse('Error bulk updating entities.', 500);
        }

        return new JsonResponse(
            [
                'count' => $updateCount,
                'ids'   => $entityIds,
                'data'  => $inputData,
            ]
        );
    }

    protected function applyBulkUpdate(
        array $entityIds,
        array $inputData
    ): int {
        $count = 0;

        foreach ($entityIds as $entityId) {
            /** @var AbstractEntity $entity */
            $entity = $this->repository->find($entityId);

            foreach ($inputData as $property => $value) {
                if ($property === 'tags') {
                    // @todo implement tags?
                    throw new BadRequestHttpException('Cannot bulk update tags.');
                }

                if (
                    $entity instanceof FinancialValueHolderInterface &&
                    FinancialValueConverter::isFinancialValue($entity, $property)
                ) {
                    $value = FinancialValueConverter::toWholeString($value);
                }

                $setter = 'set' . ucfirst($property);

                if (method_exists($entity, $setter)) {
                    $entity->{$setter}($value);
                } else {
                    throw new \LogicException(
                        sprintf(
                            "Invalid property '%s' setting attempted on '%s' entity.",
                            $property,
                            $entity::getEntityTypeCode()
                        )
                    );
                }
            }

            $count++;
        }

        return $count;
    }

    protected function getBulkUpdateClassName(string $resourceClass): string
    {
        return str_replace('Entity', 'ApiPlatform\DataTransferObject', $resourceClass) . 'PutBulk';
    }

    protected function validateInput(array $inputData, string $resourceClass): void
    {
        $bulkUpdateClass = $this->getBulkUpdateClassName($resourceClass);

        if (!class_exists($bulkUpdateClass)) {
            throw new \LogicException(
                sprintf(
                    "Missing bulk update class for the resource '%s'",
                    $resourceClass
                )
            );
        }

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $serializer = new Serializer([new ObjectNormalizer($classMetadataFactory)], [new JsonEncoder()]);
        $bulkPutObject = $serializer->deserialize(
            json_encode($inputData),
            $bulkUpdateClass,
            'json',
            [
                AbstractNormalizer::ALLOW_EXTRA_ATTRIBUTES => false,
            ]
        );

        $this->validator->validate($bulkPutObject);
    }
}
