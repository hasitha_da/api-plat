<?php

namespace SimitiveApiPlatformBundle\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use SimitiveApiPlatformBundle\Builder\AdvancedFilterBuilder;
use SimitiveApiPlatformBundle\Builder\BulkSelectionFilterBuilder;
use SimitiveBase\Model\NestedSet\NestableInterface;
use SimitiveBase\Repository\BaseEntityRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class AbstractBulkOperationAction
{
    protected BaseEntityRepositoryInterface $repository;

    protected function getEntityIds(string $resourceClass, ?array $filters): array
    {
        $queryBuilder = $this->repository->createQueryBuilder('o')
            ->select('PARTIAL o.{id}');

        if ($filters !== null) {
            $this->applyFilters($queryBuilder, $resourceClass, $filters);
        }

        // Always exclude root of nested sets
        if ((new $resourceClass()) instanceof NestableInterface) {
            $queryBuilder->andWhere('o.level != 0');
        }

        return array_map(
            static function ($id) {
                return $id['id'];
            },
            $queryBuilder->getQuery()->getArrayResult()
        );
    }

    protected function applyFilters(QueryBuilder $queryBuilder, string $resourceClass, array $filters): void
    {
        $queryNameGenerator = new QueryNameGenerator();

        foreach ($filters as $name => $filter) {
            if ($name === 'advanced') {
                $builder = new AdvancedFilterBuilder();
                $builder->build($filter, $queryBuilder, $queryNameGenerator, $resourceClass);
            } elseif ($name === 'bulkSelection') {
                $builder = new BulkSelectionFilterBuilder();
                $builder->build($filter, $queryBuilder, $queryNameGenerator, $resourceClass);
            } else {
                throw new BadRequestHttpException(
                    sprintf("Filter type '%s' is not supported for this operation.", $name)
                );
            }
        }
    }

    /**
     * @param class-string $entityClass
     */
    protected function getRepository(
        EntityManagerInterface $entityManager,
        string $entityClass
    ): BaseEntityRepositoryInterface {
        $repository = $entityManager->getRepository($entityClass);

        if ($repository instanceof BaseEntityRepositoryInterface) {
            return $repository;
        }

        throw new LogicException('Repository class must support BaseEntityRepositoryInterface.');
    }
}
