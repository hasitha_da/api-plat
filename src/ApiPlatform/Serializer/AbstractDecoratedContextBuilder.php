<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class AbstractDecoratedContextBuilder implements SerializerContextBuilderInterface
{
    public function __construct(
        protected SerializerContextBuilderInterface $decorated
    ) {
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);

        if ($normalization === false) {
            // Never allow unknown fields to be posted.
            $context['allow_extra_attributes'] = false;
        }

        if ($normalization === true) {
            // Restrict the possible depth of retrieved nested fields.
            $context['enable_max_depth'] = true;
            $context['max_depth'] = 1;
        }

        return $context;
    }
}
