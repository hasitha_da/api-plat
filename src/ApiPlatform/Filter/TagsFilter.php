<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use Ramsey\Uuid\Uuid;
use SimitiveBase\Model\Tagging\TaggableInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TagsFilter extends AbstractContextAwareFilter
{
    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if ($property !== 'tags') {
            return;
        }

        $classInstance = new $resourceClass();

        if ($classInstance instanceof TaggableInterface === false) {
            throw new LogicException('TagsFilter configured for incompatible resource.');
        }

        if (is_array($value) === false) {
            throw new BadRequestHttpException("You must specify a valid 'tags' query parameter.");
        }

        foreach ($value as $tagSetId => $searchValue) {
            if (is_string($tagSetId) === false || Uuid::isValid($tagSetId) === false) {
                throw new BadRequestHttpException("You must specify a valid 'tags' query parameter.");
            }
        }

        foreach ($value as $tagSetId => $searchValue) {
            $tagsJoinPathProperty = 'tags';
            $tagsJoinAlias = $queryNameGenerator->generateJoinAlias($tagsJoinPathProperty);
            $tagsParameterName = $queryNameGenerator->generateParameterName('tag_code');

            $tagSetJoinPathProperty = 'tagSet';
            $tagSetJoinAlias = $queryNameGenerator->generateJoinAlias($tagSetJoinPathProperty);
            $tagSetParameterName = $queryNameGenerator->generateParameterName('tag_set_id');

            $queryBuilder->leftJoin(
                sprintf('o.%s', $tagsJoinPathProperty),
                $tagsJoinAlias,
                Join::WITH,
                sprintf('%s.tagSet = :%s', $tagsJoinAlias, $tagSetParameterName)
            );
            $queryBuilder->leftJoin(
                sprintf('%s.%s', $tagsJoinAlias, $tagSetJoinPathProperty),
                $tagSetJoinAlias
            );
            $queryBuilder->andWhere(sprintf('LOWER(%s.code) LIKE :%s', $tagsJoinAlias, $tagsParameterName));
            $queryBuilder->setParameter($tagSetParameterName, $tagSetId);
            $queryBuilder->setParameter($tagsParameterName, '%' . addcslashes(strtolower($searchValue), '%_') . '%');
        }
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'tags' => [
                'property'      => 'tags[]',
                'type'          => 'string',
                'required'      => false,
                'is_collection' => true,
                'swagger'       => [
                    'description' => 'Filter by a case insensitive partial tag code within a tag set. Use the UUID of the relevant tag set in the parameter array key. Example: tags[UUID]=CODE',
                    'name'        => 'tags[]',
                    'type'        => 'array',
                ],
            ],
        ];
    }
}
