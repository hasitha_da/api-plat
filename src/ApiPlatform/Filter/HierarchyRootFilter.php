<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use SimitiveBase\Model\NestedSet\NestableInterface;

class HierarchyRootFilter extends AbstractContextAwareFilter
{
    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if ($property !== 'hideRoot') {
            return;
        }

        $classInstance = new $resourceClass();

        if (
            $classInstance instanceof NestableInterface === false
        ) {
            throw new LogicException('HierarchyRootFilter configured for incompatible resource.');
        }

        if ($value === false) {
            return;
        }

        $queryBuilder->andWhere('o.level != 0');
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        return [
            'hideRoot' => [
                'property' => 'hideRoot',
                'type'     => 'boolean',
                'required' => false,
            ],
        ];
    }
}
