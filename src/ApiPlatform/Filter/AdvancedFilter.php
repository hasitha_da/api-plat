<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use SimitiveApiPlatformBundle\Builder\AdvancedFilterBuilder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AdvancedFilter extends AbstractContextAwareFilter
{
    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if ($property !== 'advanced') {
            return;
        }

        if (!is_array($value)) {
            throw new BadRequestHttpException("The 'advanced' parameter must be an array.");
        }

        $builder = new AdvancedFilterBuilder();
        $builder->build($value, $queryBuilder, $queryNameGenerator, $resourceClass);
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        return [
            'advanced' => [
                'property' => 'advanced',
                'type'     => 'array',
                'required' => false,
                'swagger'  => [
                    'description' => 'Advanced filter for matching multiple rules.',
                    'name'        => 'advanced',
                    'type'        => 'array',
                ],
            ],
        ];
    }
}
