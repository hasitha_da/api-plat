<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use SimitiveApiPlatformBundle\Builder\BulkSelectionFilterBuilder;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BulkSelectionFilter extends AbstractContextAwareFilter
{
    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if ($property !== 'bulkSelection') {
            return;
        }

        if (!is_array($value)) {
            throw new BadRequestHttpException("The 'bulkSelection' parameter must be an array.");
        }

        $builder = new BulkSelectionFilterBuilder();
        $builder->build($value, $queryBuilder, $queryNameGenerator, $resourceClass);
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        return [
            'bulkSelection' => [
                'property' => 'bulkSelection',
                'type'     => 'array',
                'required' => false,
                'swagger'  => [
                    'description' => 'Bulk selection filter for fetching specific items.',
                    'name'        => 'bulkSelection',
                    'type'        => 'array',
                ],
            ],
        ];
    }
}
