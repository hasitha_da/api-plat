<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use SimitiveSecurityBundle\Security\Model\SimitiveSecurityUserInterface;

class UserFullNameSearchFilter extends AbstractContextAwareFilter
{
    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if ($property !== 'userFullNameSearch') {
            return;
        }

        $classInstance = new $resourceClass();

        if (
            $classInstance instanceof SimitiveSecurityUserInterface === false
        ) {
            throw new LogicException('UserFullNameFilter configured for incompatible resource.');
        }

        $parameterName = $queryNameGenerator->generateParameterName(':fullName');
        $like = $queryBuilder->expr()->like('LOWER(CONCAT(o.firstName, \' \', o.lastName))', $parameterName);
        $queryBuilder->andWhere($like);
        $queryValue = strtolower('%' . $value . '%');
        $queryBuilder->setParameter($parameterName, $queryValue);
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        return [
            'fullName' => [
                'property' => 'fullName',
                'type'     => 'string',
                'required' => false,
                'swagger'  => [
                    'description' => 'Search users by firstName and lastName together.',
                    'name'        => 'fullName',
                    'type'        => 'string',
                ],
            ],
        ];
    }
}
