<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use SimitiveSecurityBundle\Security\Model\SimitiveSecurityUserInterface;

class UserSearchFilter extends AbstractContextAwareFilter
{
    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if ($property !== 'userSearch') {
            return;
        }

        $classInstance = new $resourceClass();

        if (
            $classInstance instanceof SimitiveSecurityUserInterface === false
        ) {
            throw new LogicException('UserSearchFilter configured for incompatible resource.');
        }

        $parameter = ':' . $queryNameGenerator->generateParameterName('term');

        $like1 = $queryBuilder->expr()->like('LOWER(o.employeeId)', $parameter);
        $like2 = $queryBuilder->expr()->like('LOWER(o.firstName)', $parameter);
        $like3 = $queryBuilder->expr()->like('LOWER(o.lastName)', $parameter);
        $like4 = $queryBuilder->expr()->like('LOWER(CONCAT(o.firstName, \' \', o.lastName))', $parameter);

        $queryBuilder->orWhere($like1);
        $queryBuilder->orWhere($like2);
        $queryBuilder->orWhere($like3);
        $queryBuilder->orWhere($like4);

        $queryValue = strtolower('%' . $value . '%');

        $queryBuilder->setParameter($parameter, $queryValue);
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        return [
            'userSearch' => [
                'property' => 'userSearch',
                'type'     => 'string',
                'required' => false,
                'swagger'  => [
                    'description' => 'Search users by employeeId, firstName or lastName.',
                    'name'        => 'userSearch',
                    'type'        => 'string',
                ],
            ],
        ];
    }
}
