<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter as CoreSearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use SimitiveBase\Model\AbstractEntity;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\Type;

class SearchFilter extends CoreSearchFilter
{
    public function __construct(
        private PropertyMetadataFactoryInterface $propertyMetadataFactory,
        ManagerRegistry $managerRegistry,
        ?RequestStack $requestStack,
        IriConverterInterface $iriConverter,
        PropertyAccessorInterface $propertyAccessor = null,
        LoggerInterface $logger = null,
        array $properties = null
    ) {
        parent::__construct(
            $managerRegistry,
            $requestStack,
            $iriConverter,
            $propertyAccessor,
            $logger,
            $properties
        );
    }

    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if (
            null === $value ||
            !$this->isPropertyEnabled($property, $resourceClass) ||
            !$this->isPropertyMapped($property, $resourceClass, true)
        ) {
            return;
        }

        // Use metadata to check if it's an object relation and therefore should be a UUID value.
        $propertyMetadata = $this->propertyMetadataFactory->create($resourceClass, $property);

        if ($propertyMetadata->getType() !== null) {
            if (
                is_numeric($value) === false &&
                $propertyMetadata->getType()->getBuiltinType() === Type::BUILTIN_TYPE_FLOAT
            ) {
                throw new BadRequestHttpException(
                    sprintf('Invalid float value passed to the "%s" query parameter.', $property)
                );
            }

            if (
                is_numeric($value) === false &&
                $propertyMetadata->getType()->getBuiltinType() === Type::BUILTIN_TYPE_INT
            ) {
                throw new BadRequestHttpException(
                    sprintf('Invalid integer value passed to the "%s" query parameter.', $property)
                );
            }

            if ($propertyMetadata->getType()->getClassName() !== null) {
                $propertyClassName = $propertyMetadata->getType()->getClassName();

                $propertyClass = new $propertyClassName();

                if ($propertyClass instanceof AbstractEntity) {
                    if (!is_array($value)) {
                        $filterValues = [$value];
                    } else {
                        $filterValues = $value;
                    }

                    foreach ($filterValues as $filterValue) {
                        if (Uuid::isValid($filterValue) === false) {
                            throw new BadRequestHttpException(
                                sprintf('Invalid UUID value passed to the "%s" query parameter.', $property)
                            );
                        }
                    }
                }
            }
        }

        parent::filterProperty($property, $value, $queryBuilder, $queryNameGenerator, $resourceClass, $operationName);
    }
}
