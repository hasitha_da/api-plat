<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use SimitiveBase\Model\CodenameableInterface;

class CodenameableMultiSearchFilter extends AbstractContextAwareFilter
{
    /* @phpstan-ignore-next-line */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ): void {
        if ($property !== 'nameSearch') {
            return;
        }

        $classInstance = new $resourceClass();

        if (
            $classInstance instanceof CodenameableInterface === false
        ) {
            throw new LogicException('CodenameableMultiSearchFilter configured for incompatible resource.');
        }

        $parameter = ':' . $queryNameGenerator->generateParameterName('term');

        $like1 = $queryBuilder->expr()->like('LOWER(o.code)', $parameter);
        $like2 = $queryBuilder->expr()->like('LOWER(o.name)', $parameter);
        $like3 = $queryBuilder->expr()->like('LOWER(o.shortName)', $parameter);

        $orWhere = $queryBuilder->expr()->orX();
        $orWhere->add($like1);
        $orWhere->add($like2);
        $orWhere->add($like3);

        $queryBuilder->andWhere($orWhere);

        $queryValue = strtolower('%' . $value . '%');

        $queryBuilder->setParameter($parameter, $queryValue);
    }

    /** {@inheritdoc} */
    public function getDescription(string $resourceClass): array
    {
        return [
            'userSearch' => [
                'property' => 'nameSearch',
                'type'     => 'string',
                'required' => false,
                'swagger'  => [
                    'description' => 'Search entities by code, name or shortName.',
                    'name'        => 'nameSearch',
                    'type'        => 'string',
                ],
            ],
        ];
    }
}
