<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\ItemDataProvider;
use ApiPlatform\Core\DataProvider\DenormalizedIdentifiersAwareItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

class UuidValidationItemDataProvider implements DenormalizedIdentifiersAwareItemDataProviderInterface, RestrictedDataProviderInterface
{
    public function __construct(private ItemDataProvider $decorated)
    {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $this->decorated->supports($resourceClass, $operationName, $context);
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        if (isset($context['has_identifier_converter']) === false && Uuid::isValid($id) === false) {
            throw new UnexpectedValueException(
                sprintf('Invalid UUID "%s" given for "%s" resource.', $id, $resourceClass)
            );
        }

        return $this->decorated->getItem($resourceClass, $id, $operationName, $context);
    }
}
