<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Normalizer;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use ApiPlatform\Core\Serializer\ItemNormalizer;
use Symfony\Component\Debug\Exception\ClassNotFoundException;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractDecoratedItemNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface, CacheableSupportsMethodInterface
{
    public function __construct(protected ItemNormalizer $decorated, protected ValidatorInterface $validator)
    {
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $this->decorated->supportsDenormalization($data, $type, $format);
    }

    public function denormalize($data, $type, $format = null, array $context = [])
    {
        if (!isset($context['input']['class'])) {
            throw new \LogicException('All API resources must have a defined input class.');
        }

        if (is_array($data) && count($data) > 0) {
            $this->validatePostData($data, $context);
        }

        return $this->decorated->denormalize($data, $type, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, $format = null, array $context = [])
    {
        return $this->decorated->normalize($object, $format, $context);
    }

    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->decorated->setSerializer($serializer);
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return $this->decorated->hasCacheableSupportsMethod();
    }

    protected function validatePostData(array $data, array $context): void
    {
        $violationList = new ConstraintViolationList();

        $inputClass = $context['input']['class'];

        try {
            $input = new $inputClass();

            foreach ($data as $property => $dataValue) {
                if (property_exists($input, $property)) {
                    $input->{$property} = $dataValue;
                } else {
                    $violationList->add(
                        new ConstraintViolation(
                            sprintf('The field "%s" is not recognised.', $property),
                            null,
                            [],
                            $input,
                            $property,
                            $dataValue
                        )
                    );
                }
            }
        } catch (ClassNotFoundException $e) {
            throw new \LogicException('Input class "' . $inputClass . '" does not exist.');
        }

        if (!$this->validator->hasMetadataFor($inputClass)) {
            throw new \LogicException('Validation metadata for "' . $inputClass . '" does not exist.');
        }

        /** @var ClassMetadata $metadata */
        $metadata = $this->validator->getMetadataFor($inputClass);

        if (empty($metadata->members)) {
            throw new \LogicException('Validation configuration for the "' . $inputClass . '" class does not exist.');
        }

        $violationList->addAll($this->validator->validate($input));

        if (count($violationList) > 0) {
            throw new ValidationException($violationList);
        }
    }
}
