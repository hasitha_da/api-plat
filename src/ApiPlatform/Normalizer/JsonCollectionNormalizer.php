<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Normalizer;

use ApiPlatform\Core\Api\ResourceClassResolverInterface;
use ApiPlatform\Core\DataProvider\PaginatorInterface;
use ApiPlatform\Core\DataProvider\PartialPaginatorInterface;
use ApiPlatform\Core\JsonLd\ContextBuilderInterface;
use ApiPlatform\Core\JsonLd\Serializer\JsonLdContextTrait;
use ApiPlatform\Core\Serializer\ContextTrait;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

/**
 * Modified version of ApiPlatform\Core\Hydra\Serializer\CollectionNormalizer.
 */
class JsonCollectionNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface, CacheableSupportsMethodInterface
{
    use ContextTrait;
    use JsonLdContextTrait;
    use NormalizerAwareTrait;

    public const FORMAT = 'json';

    public function __construct(
        protected ContextBuilderInterface $contextBuilder,
        protected ResourceClassResolverInterface $resourceClassResolver
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null, $context = [])
    {
        return self::FORMAT === $format && is_iterable($data);
    }

    /**
     * {@inheritdoc}
     *
     * @param iterable $object
     */
    public function normalize($object, $format = null, array $context = [])
    {
        if (isset($context['resource_class'])) {
            $resourceClass = $this->resourceClassResolver->getResourceClass($object, $context['resource_class']);

            if ($object instanceof PersistentCollection && $object->getTypeClass()->getName(
                ) !== $context['resource_class']) {
                $context = $this->initContext($resourceClass, $context);
            }
        }

        $data = [];

        if (!isset($context['resource_class']) || isset($context['api_sub_level'])) {
            return $this->normalizeRawCollection($object, $format, $context);
        }

        $data['items'] = [];

        foreach ($object as $obj) {
            $data['items'][] = $this->normalizer->normalize($obj, $format, $context);
        }

        if ($object instanceof PaginatorInterface) {
            $data['count'] = $object->getTotalItems();
        }

        if (is_array($object) || ($object instanceof \Countable && !$object instanceof PartialPaginatorInterface)) {
            $data['count'] = count($object);
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }

    /**
     * Normalizes a raw collection (not API resources).
     */
    private function normalizeRawCollection(iterable $object, ?string $format, array $context): array
    {
        $data = [];

        foreach ($object as $index => $obj) {
            $data[$index] = $this->normalizer->normalize($obj, $format, $context);
        }

        return $data;
    }
}
