<?php

/*
 * This file is part of the API Platform project.
 *
 * (c) Kévin Dunglas <dunglas@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SimitiveApiPlatformBundle\ApiPlatform\Normalizer;

use ApiPlatform\Core\Exception\InvalidIdentifierException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

/**
 * Custom version of ApiPlatform\Core\Bridge\RamseyUuid\Identifier\Normalizer/UuidNormalizer.
 */
class UuidNormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (Uuid::isValid($data) === false) {
            throw new InvalidIdentifierException('String is not a valid UUID.');
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        // String identifiers should always be a UUID.
        return $type === 'string';
    }
}
