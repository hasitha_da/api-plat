<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class AbstractDocumentationNormalizer implements NormalizerInterface
{
    public function __construct(protected NormalizerInterface $decorated, protected string $apiEndpoint)
    {
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        $prefixedDocPaths = [];

        foreach ($docs['paths'] as $pathUrl => &$path) {
            // Adapt any auto-generated collection schema to match JsonCollectionNormalizer class output.
            if (str_contains($pathUrl, '{id}')) {
                $this->decorateCollectionPaths($path);
            }

            $prefixedDocPaths[$this->apiEndpoint . $pathUrl] = $path;
        }

        $docs['paths'] = $prefixedDocPaths;

        return $docs;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    protected function decorateCollectionPaths(array &$path): void
    {
        // No foolproof way to tell if it's a get collection path, so a few checks are needed.
        if (!isset($path['get']['responses'][200])) {
            return;
        }

        if (!str_contains($path['get']['responses'][200]['description'], 'collection response')) {
            return;
        }

        $originalCollectionSchema = $path['get']['responses'][200]['schema'];

        $path['get']['responses'][200]['schema'] = [
            'type'       => 'object',
            'properties' => [
                'items' => $originalCollectionSchema,
                'count' => [
                    'type' => 'integer',
                ],
            ],
        ];
    }
}
