<?php

namespace SimitiveApiPlatformBundle\ApiPlatform\Normalizer;

use SimitiveBase\Model\AbstractEntity;
use SimitiveTranslationsBundle\Entity\Translation\Language;
use SimitiveTranslationsBundle\Provider\TranslationsProvider;
use SimitiveTranslationsBundle\Validation\TranslatableConstraintInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Modified version of ApiPlatform\Core\Problem\Serializer\ConstraintViolationListNormalizer.
 */
class ConstraintViolationListNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    public const FORMAT = 'jsonproblem';
    public const TYPE = 'type';
    public const TITLE = 'title';

    private array $defaultContext = [
        self::TYPE  => 'https://tools.ietf.org/html/rfc2616#section-10',
        self::TITLE => 'An error occurred',
    ];

    private ?array $serializePayloadFields;

    private ?NameConverterInterface $nameConverter;

    private ?TranslationsProvider $translationsProvider;

    private Language $defaultLanguage;

    public function __construct(
        array $serializePayloadFields = null,
        NameConverterInterface $nameConverter = null,
        array $defaultContext = [],
        TranslationsProvider $translationsProvider = null
    ) {
        $this->serializePayloadFields = $serializePayloadFields;
        $this->nameConverter = $nameConverter;
        $this->defaultContext = array_merge($this->defaultContext, $defaultContext);
        $this->translationsProvider = $translationsProvider;
        $this->defaultLanguage = $this->translationsProvider->getDefaultLanguage();
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return static::FORMAT === $format && $data instanceof ConstraintViolationListInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        [$messages, $violations] = $this->getMessagesAndViolations($object);

        return [
            'type'       => $context[self::TYPE] ?? $this->defaultContext[self::TYPE],
            'title'      => $context[self::TITLE] ?? $this->defaultContext[self::TITLE],
            'detail'     => $messages ? implode("\n", $messages) : (string) $object,
            'violations' => $violations,
        ];
    }

    protected function getMessagesAndViolations(ConstraintViolationListInterface $constraintViolationList): array
    {
        $violations = $messages = [];

        /** @var ConstraintViolation $violation */
        foreach ($constraintViolationList as $violation) {
            $class = \is_object($root = $violation->getRoot()) ? \get_class($root) : null;
            $propertyPath = $this->translatePropertyPath(
                $this->defaultLanguage->getCode(),
                $this->nameConverter->normalize($violation->getPropertyPath()),
                $class
            );

            $message = $violation->getMessage();

            if ($violation->getConstraint() instanceof TranslatableConstraintInterface) {
                $message = $this->translationsProvider->translate(
                    $this->defaultLanguage->getCode(),
                    $violation->getMessageTemplate(),
                    $violation->getParameters()
                );
            }

            $violationData = [
                'propertyPath' => $propertyPath,
                'message'      => $message,
            ];

            $constraint = $violation->getConstraint();

            if ($this->serializePayloadFields && $constraint && $constraint->payload) {
                // If some fields are whitelisted, only them are added
                $payloadFields = array_intersect_key(
                    $constraint->payload,
                    array_flip($this->serializePayloadFields)
                );
                $violationData['payload'] = $payloadFields;
            }

            $violations[] = $violationData;
            $messages[] = ($violationData['propertyPath'] ? "{$violationData['propertyPath']}: " : '') . $violationData['message'];
        }

        return [$messages, $violations];
    }

    protected function translatePropertyPath(string $languageCode, string $propertyPath, string $class = null): string
    {
        if ($class === null) {
            return $propertyPath;
        }

        if (defined(sprintf('%s::ENTITY_CLASS', $class))) {
            $class = $class::ENTITY_CLASS;
        } elseif (str_contains($class, 'DataTransferObject')) {
            $class = str_replace('ApiPlatform\DataTransferObject', 'Entity', $class);
            $class = preg_replace('/Post|Put$/', '', $class);
        }

        if (class_exists($class) === false) {
            throw new \LogicException('Invalid class name used during validation error translation.');
        }

        $classInstance = new $class();

        if ($classInstance instanceof AbstractEntity) {
            $type = $classInstance::getEntityTypeCode();
            $property = strtolower(
                preg_replace(
                    '/(?<!^)[A-Z]/',
                    '_$0',
                    $propertyPath
                )
            );

            $translation = $this->translationsProvider->translate(
                $languageCode,
                sprintf('entity.%s.field.%s.name', $type, $property),
                [],
                false
            );

            if ($translation !== null) {
                return $translation;
            }
        }

        return $propertyPath;
    }
}
