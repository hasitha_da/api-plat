<?php

namespace SimitiveApiPlatformBundle\Builder;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use Ramsey\Uuid\Uuid;
use SimitiveBase\Model\AbstractEntity;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BulkSelectionFilterBuilder
{
    public function build(
        array $parameters,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
    ): void {
        $property = 'id';
        $classInstance = new $resourceClass();

        if ($classInstance instanceof AbstractEntity === false) {
            throw new LogicException('BulkSelectionFilter configured for incompatible resource.');
        }

        $allSelected = false;
        $toggleIds = [];

        if (!isset($parameters['toggle']) && !isset($parameters['allSelected'])) {
            throw new BadRequestHttpException("You must specify 'toggle' and/or 'allSelected' parameters.");
        }

        if (isset($parameters['allSelected'])) {
            $allSelected = filter_var($parameters['allSelected'], FILTER_VALIDATE_BOOLEAN);

            if (!is_bool($allSelected)) {
                throw new BadRequestHttpException("The 'allSelected' parameter must resolve to a boolean value.");
            }
        }

        if (isset($parameters['toggle'])) {
            $toggleIds = $parameters['toggle'];

            if (!is_array($toggleIds)) {
                throw new BadRequestHttpException("You must specify a 'toggle' array parameter.");
            }
        }

        foreach ($toggleIds as $toggleId) {
            if (Uuid::isValid($toggleId) === false) {
                throw new BadRequestHttpException("Invalid UUID found in 'toggle' array parameter.");
            }
        }

        if ($allSelected) {
            // If ‘allSelected’ and no ‘toggle’ entries exist, get all entities
            if (empty($toggleIds)) {
                return;
            }

            // If ‘allSelected’ and ‘toggle’ entries exist, get all entities except those
            $parameterName = $queryNameGenerator->generateParameterName($property);
            $queryBuilder->andWhere(sprintf('o.%s NOT IN (:%s)', $property, $parameterName));
        } else {
            // If ‘allSelected’ is false and no ‘toggle’ entries exist, get no entities
            if (empty($toggleIds)) {
                $queryBuilder->andWhere(sprintf('o.%s IS NULL', $property));

                return;
            }

            // If ‘allSelected’ is false and ‘toggle’ entries exist, get only those entities
            $parameterName = $queryNameGenerator->generateParameterName($property);
            $queryBuilder->andWhere(sprintf('o.%s IN (:%s)', $property, $parameterName));
        }

        $queryBuilder->setParameter($parameterName, $toggleIds);
    }
}
