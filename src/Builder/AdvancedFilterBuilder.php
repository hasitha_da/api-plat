<?php

namespace SimitiveApiPlatformBundle\Builder;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use DateTime;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use LogicException;
use Ramsey\Uuid\Uuid;
use SimitiveBase\Helper\FinancialValueConverter;
use SimitiveBase\Model\AbstractEntity;
use SimitiveBase\Model\FinancialValueHolderInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AdvancedFilterBuilder
{
    public const MATCH_AND = 'and';
    public const MATCH_OR = 'or';

    public const OPERATOR_IS = 'is';
    public const OPERATOR_IS_NOT = 'is_not';
    public const OPERATOR_IS_EMPTY = 'is_empty';
    public const OPERATOR_IS_NOT_EMPTY = 'is_not_empty';

    public const OPERATOR_CONTAINS = 'contains';
    public const OPERATOR_DOES_NOT_CONTAIN = 'does_not_contain';
    public const OPERATOR_STARTS_WITH = 'starts_with';
    public const OPERATOR_ENDS_WITH = 'ends_with';

    public const OPERATOR_GREATER_THAN = 'greater_than';
    public const OPERATOR_LESS_THAN = 'less_than';
    public const OPERATOR_BETWEEN = 'between';

    public const OPERATOR_IS_BEFORE = 'is_before';
    public const OPERATOR_IS_AFTER = 'is_after';

    public const OPERATOR_IS_ONE_OF = 'is_one_of';
    public const OPERATOR_IS_NOT_ONE_OF = 'is_not_one_of';

    public static function getValidOperators(): array
    {
        return [
            self::OPERATOR_IS,
            self::OPERATOR_IS_NOT,
            self::OPERATOR_IS_EMPTY,
            self::OPERATOR_IS_NOT_EMPTY,
            self::OPERATOR_CONTAINS,
            self::OPERATOR_DOES_NOT_CONTAIN,
            self::OPERATOR_STARTS_WITH,
            self::OPERATOR_ENDS_WITH,
            self::OPERATOR_GREATER_THAN,
            self::OPERATOR_LESS_THAN,
            self::OPERATOR_BETWEEN,
            self::OPERATOR_IS_BEFORE,
            self::OPERATOR_IS_AFTER,
            self::OPERATOR_IS_ONE_OF,
            self::OPERATOR_IS_NOT_ONE_OF,
        ];
    }

    public function build(
        array $parameters,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
    ): void {
        $classInstance = new $resourceClass();

        if ($classInstance instanceof AbstractEntity === false) {
            throw new LogicException('AdvancedFilter configured for incompatible resource.');
        }

        /** @phpstan-ignore-next-line */
        $classMetadata = $queryBuilder->getEntityManager()->getClassMetadata($resourceClass);

        foreach ($parameters['rules'] as &$rule) {
            $field = $rule['field'];
            $operator = $rule['operator'];
            $value = $rule['value'] ?? null;

            if (
                $classInstance instanceof FinancialValueHolderInterface &&
                FinancialValueConverter::isFinancialValue($classInstance, $field)
            ) {
                if ($operator === self::OPERATOR_BETWEEN) {
                    $rule['value'][0] = FinancialValueConverter::toWholeString($value[0]);
                    $rule['value'][1] = FinancialValueConverter::toWholeString($value[1]);
                } else {
                    $rule['value'] = FinancialValueConverter::toWholeString($value);
                }
            }
        }
        unset($rule);

        $parameters = $this->validateParameters($parameters, $classMetadata, $queryBuilder);
        $match = $parameters['match'];

        $expression = match ($match) {
            self::MATCH_AND => $queryBuilder->expr()->andX(),
            self::MATCH_OR => $queryBuilder->expr()->orX(),
            default => throw new LogicException('AdvancedFilter provided invalid match type.')
        };

        foreach ($parameters['rules'] as $rule) {
            $field = $rule['field'];
            $fieldType = $rule['fieldType'];
            $operator = $rule['operator'];
            $joinBy = $rule['joinBy'] ?? null;
            $value = $rule['value'] ?? null;
            $fieldParameterName = $queryNameGenerator->generateParameterName($field);

            if ($joinBy !== null) {
                $alias = $queryNameGenerator->generateJoinAlias($joinBy);
                $queryBuilder->join(sprintf('%s.%s', 'o', $joinBy), $alias);
            } else {
                $alias = 'o';
            }

            switch ($operator) {
                case self::OPERATOR_IS:
                    if ($fieldType === 'datetimetz' || $fieldType === 'datetime') {
                        $expression->add(
                            sprintf('DATE_TRUNC(\'day\', %s.%s) = :%s', $alias, $field, $fieldParameterName)
                        );
                    } else {
                        $expression->add(sprintf('%s.%s = :%s', $alias, $field, $fieldParameterName));
                    }

                    break;
                case self::OPERATOR_IS_NOT:
                    if ($fieldType === 'datetimetz' || $fieldType === 'datetime') {
                        $expression->add(
                            sprintf('DATE_TRUNC(\'day\', %s.%s)  != :%s', $alias, $field, $fieldParameterName)
                        );
                    } else {
                        $expression->add(sprintf('%s.%s != :%s', $alias, $field, $fieldParameterName));
                    }

                    break;
                case self::OPERATOR_STARTS_WITH:
                    $expression->add(sprintf('LOWER(%s.%s) LIKE :%s', $alias, $field, $fieldParameterName));
                    $value = strtolower($value) . '%';

                    break;
                case self::OPERATOR_ENDS_WITH:
                    $expression->add(sprintf('LOWER(%s.%s) LIKE :%s', $alias, $field, $fieldParameterName));
                    $value = '%' . strtolower($value);

                    break;
                case self::OPERATOR_CONTAINS:
                    $expression->add(sprintf('LOWER(%s.%s) LIKE :%s', $alias, $field, $fieldParameterName));
                    $value = '%' . strtolower($value) . '%';

                    break;
                case self::OPERATOR_DOES_NOT_CONTAIN:
                    $expression->add(sprintf('LOWER(%s.%s) NOT LIKE :%s', $alias, $field, $fieldParameterName));
                    $value = '%' . strtolower($value) . '%';

                    break;
                case self::OPERATOR_IS_BEFORE:
                case self::OPERATOR_LESS_THAN:
                    $expression->add(sprintf('%s.%s < :%s', $alias, $field, $fieldParameterName));

                    break;
                case self::OPERATOR_IS_AFTER:
                case self::OPERATOR_GREATER_THAN:
                    $expression->add(sprintf('%s.%s > :%s', $alias, $field, $fieldParameterName));

                    break;
                case self::OPERATOR_BETWEEN:
                    $fieldParameterName2 = $queryNameGenerator->generateParameterName($field);
                    $expression->add(
                        sprintf('%s.%s BETWEEN :%s AND :%s', $alias, $field, $fieldParameterName, $fieldParameterName2)
                    );
                    $queryBuilder->setParameter($fieldParameterName, $value[0]);
                    $queryBuilder->setParameter($fieldParameterName2, $value[1]);
                    $value = null;

                    break;
                case self::OPERATOR_IS_ONE_OF:
                    $expression->add(sprintf('%s.%s IN (:%s)', $alias, $field, $fieldParameterName));

                    break;
                case self::OPERATOR_IS_NOT_ONE_OF:
                    $expression->add(sprintf('%s.%s NOT IN (:%s)', $alias, $field, $fieldParameterName));

                    break;
                case self::OPERATOR_IS_EMPTY:
                    $expression->add(sprintf('%s.%s IS NULL', $alias, $field));

                    break;
                case self::OPERATOR_IS_NOT_EMPTY:
                    $expression->add(sprintf('%s.%s IS NOT NULL', $alias, $field));

                    break;
                default:
                    throw new LogicException(sprintf("Missing '%s' operator case.", $rule['operator']));
            }

            if ($value !== null) {
                $queryBuilder->setParameter($fieldParameterName, $value);
            }
        }

        $queryBuilder->andWhere($expression);
    }

    protected function validateParameters(
        array $parameters,
        ClassMetadata $entityMetaData,
        QueryBuilder $queryBuilder
    ): array {
        if (
            !isset($parameters['match']) ||
            !in_array($parameters['match'], [self::MATCH_AND, self::MATCH_OR], true)
        ) {
            throw new BadRequestHttpException("You must specify a valid 'match' parameter.");
        }

        if (
            !isset($parameters['rules']) ||
            count($parameters['rules']) === 0
        ) {
            throw new BadRequestHttpException("You must specify a valid array within the 'rules' parameter.");
        }

        foreach ($parameters['rules'] as &$rule) {
            if (!isset($rule['field'])) {
                throw new BadRequestHttpException("You must specify a 'field' parameter for each rule.");
            }

            $fieldInput = $rule['field'];
            $relationFields = explode('.', $fieldInput);
            $field = $relationFields[0];
            $relationField = $relationFields[1] ?? null;

            if (isset($entityMetaData->fieldMappings[$field])) {
                if ($relationField !== null) {
                    throw new BadRequestHttpException(
                        sprintf("The nested field parameter '%s' is not recognised.", $fieldInput)
                    );
                }

                $fieldType = $entityMetaData->fieldMappings[$field]['type'];
            } elseif (
                isset($entityMetaData->associationMappings[$field]) &&
                $entityMetaData->associationMappings[$field]['type'] === ClassMetadata::MANY_TO_ONE
            ) {
                if ($relationField !== null) {
                    $fieldMetadata = $entityMetaData->associationMappings[$field];

                    /** @phpstan-ignore-next-line */
                    $joinClassMetadata = $queryBuilder->getEntityManager()->getClassMetadata(
                        $fieldMetadata['targetEntity']
                    );

                    if (isset($joinClassMetadata->fieldMappings[$relationField])) {
                        $rule['field'] = $relationField;
                        $rule['joinBy'] = $field;
                        $fieldType = $joinClassMetadata->fieldMappings[$relationField]['type'];
                    } elseif (isset($joinClassMetadata->associationMappings[$relationField])) {
                        $rule['field'] = $relationField;
                        $rule['joinBy'] = $field;
                        $fieldType = 'uuid';
                    } else {
                        throw new BadRequestHttpException(
                            sprintf("The nested field parameter '%s' is not recognised.", $fieldInput)
                        );
                    }
                } else {
                    $fieldType = 'uuid';
                }
            } elseif (isset($entityMetaData->associationMappings[$field])) {
                if ($relationField !== null) {
                    throw new BadRequestHttpException(
                        sprintf("The nested field parameter '%s' is not recognised.", $fieldInput)
                    );
                }

                $rule['field'] = 'id';
                $rule['joinBy'] = $entityMetaData->associationMappings[$field]['fieldName'];
                $fieldType = 'uuid';
            } else {
                throw new BadRequestHttpException(sprintf("The field parameter '%s' is not supported.", $fieldInput));
            }

            $rule['fieldType'] = $fieldType;

            if (
                !isset($rule['operator']) ||
                !in_array($rule['operator'], self::getValidOperators(), true)
            ) {
                throw new BadRequestHttpException("You must specify a valid 'operator' parameter for each rule.");
            }

            $operator = $rule['operator'];

            if (in_array($operator, [self::OPERATOR_IS_EMPTY, self::OPERATOR_IS_NOT_EMPTY])) {
                if (isset($rule['value'])) {
                    throw new BadRequestHttpException(
                        "You must not specify a 'value' parameter when using 'is_empty' or 'is_not_empty' operators."
                    );
                }

                continue;
            }

            if (!isset($rule['value'])) {
                throw new BadRequestHttpException("You must specify a 'value' parameter for each rule.");
            }

            $value = $rule['value'];

            if (
                $operator === self::OPERATOR_BETWEEN &&
                (!is_array($value) || count($value) !== 2)
            ) {
                throw new BadRequestHttpException(
                    sprintf(
                        "You must provide an array of two values for the field '%s' when using the 'between' operator.",
                        $field
                    )
                );
            }

            if (
                ($operator === self::OPERATOR_IS_ONE_OF || $operator === self::OPERATOR_IS_NOT_ONE_OF) &&
                (!is_array($value) || count($value) === 0)
            ) {
                throw new BadRequestHttpException(
                    sprintf(
                        "You must provide an array of values for the field '%s' when using the 'is_one_of' or 'is_not_one_of' operator.",
                        $field
                    )
                );
            }

            if (is_array($value)) {
                foreach ($value as $item) {
                    $this->validateValue($fieldType, $field, $item);
                }
            } else {
                $this->validateValue($fieldType, $field, $value);
            }
        }

        return $parameters;
    }

    protected function validateValue(string $fieldType, string $fieldName, mixed $value): void
    {
        if (is_array($value)) {
            throw new BadRequestHttpException(
                sprintf("The value for the field '%s' cannot be an array.", $fieldName)
            );
        }

        if (!in_array(
            $fieldType,
            [
                'uuid',
                'text',
                'citext',
                'integer',
                'bigint',
                'float',
                'boolean',
                'date',
                'datetime',
                'datetimetz',
            ]
        )) {
            throw new BadRequestHttpException(
                sprintf("The field parameter '%s' is not supported by this filter.", $fieldName)
            );
        }

        switch ($fieldType) {
            case 'uuid':
                if (!Uuid::isValid($value)) {
                    throw new BadRequestHttpException(
                        sprintf("The field parameter '%s' must be a string.", $fieldName)
                    );
                }

                break;
            case 'text':
            case 'citext':
                if (!is_string($value)) {
                    throw new BadRequestHttpException(
                        sprintf("The field parameter '%s' must be a string.", $fieldName)
                    );
                }

                break;
            case 'boolean':
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);

                if (!is_bool($value)) {
                    throw new BadRequestHttpException(
                        sprintf("The field parameter '%s' must be a boolean.", $fieldName)
                    );
                }

                break;
            case 'integer':
            case 'bigint':
                if (!is_numeric($value) || str_contains($value, '.')) {
                    throw new BadRequestHttpException(
                        sprintf("The field parameter '%s' must be an integer.", $fieldName)
                    );
                }

                break;
            case 'float':
                if (!is_numeric($value)) {
                    throw new BadRequestHttpException(
                        sprintf("The field parameter '%s' must be a float.", $fieldName)
                    );
                }

                break;
            case 'date':
            case 'datetime':
            case 'datetimetz':
                if (!$this->validateDate($value)) {
                    throw new BadRequestHttpException(
                        sprintf("The field parameter '%s' must be an ISO date string.", $fieldName)
                    );
                }

                break;
            default:
                throw new LogicException(sprintf("Validation missing for field type '%s'.", $fieldName));
        }
    }

    protected function validateDate(string $date, string $format = 'Y-m-d'): bool
    {
        $d = DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) === $date;
    }
}
