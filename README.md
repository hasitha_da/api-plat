# Simitive API Platform Bundle

Bundle with common API Platform functionality to be used in other products.

## Local development dependencies

* Run: `docker run --rm -v $(pwd):/usr/src/library php:8.0.8-cli-alpine3.13 /bin/sh -c 'curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.0.12 && cd /usr/src/library && composer install'`
